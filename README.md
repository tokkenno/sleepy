# Sleepy

Multiplatform kad/edonkey/? client

## Goals

- [ ] JVM Daemon (Headless, HTTP)
  - [ ] Define API to support multi-network downloads/uploads
  - [ ] Implement a basic Kad network client (Download/Upload files)
  - [ ] Implement a basic eDonkey/eMule network client (Download/Upload files)
  - [ ] Define API to support multi-network file search
  - [ ] Implement file search over the eDonkey/eMule network
  - [ ] Implement file search over the Kad network
  - [ ] Define API to support multiple client APIs
  - [ ] Implement HTTP client API (REST, WS)
    - [ ] Implement basic web UI
- [ ] Add support to Kotlin Multiplatform
- [ ] Native Daemon (Headless, HTTP)
  - [ ] Linux (x86, arm64)
  - [ ] Windows (x86)
- [ ] Add support to work in library mode (To be used by other projects)
- [ ] Improve web UI
- [ ] Implement new P2P protocol over HTTP (In draft...)
