package com.aitorgf.sleepy.net.kad.router

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.net.kad.Peer
import com.aitorgf.sleepy.types.UInt128
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

internal class KBucket<T : Peer> : MutableCollection<T>, RouterCollection<T> {
    companion object {
        /** Max number of peers in each k-bucket */
        const val MaxBucketSize = 16

        /** Number of peers permitted from same public IP */
        const val MaxPeersPerIp = 3
    }

    private val peers: MutableSet<T> = LinkedHashSet()
    private val peersAccess: Lock = ReentrantLock()

    override val size: Int
        get() = this.peers.size

    val left: Int
        get() = MaxBucketSize - this.peers.size

    fun isFull(): Boolean {
        return this.left <= 0
    }

    operator fun get(id: UInt128): T? {
        this.peersAccess.withLock {
            return this.peers.firstOrNull { it.kadId == id }
        }
    }

    operator fun get(ip: IPv4): T? {
        this.peersAccess.withLock {
            return this.peers.firstOrNull { it.ipv4 == ip }
        }
    }

    override fun random(): T? {
        this.peersAccess.withLock {
            if (this.peers.isEmpty()) {
                return null
            }
            return this.peers.random()
        }
    }

    fun oldest(): T? {
        this.peersAccess.withLock {
            return this.peers.firstOrNull()
        }
    }

    override fun add(element: T): Boolean {
        this.peersAccess.withLock {
            var sameNetwork = 0
            this.peers.forEach { existingPeer ->
                if (existingPeer.kadId == element.kadId) {
                    return false
                }
                if (existingPeer.ipv4 == element.ipv4) {
                    sameNetwork++
                }
            }

            if (this.peers.size >= MaxBucketSize) {
                throw Exception("Bucket lleno")
            }

            if (sameNetwork >= MaxPeersPerIp) {
                throw Exception("Demasiadas IPs en la misma red")
            }

            return this.peers.add(element)
        }
    }

    override fun remove(element: T): Boolean {
        this.peersAccess.withLock {
            return this.peers.remove(element)
        }
    }

    fun remove(id: UInt128): Boolean {
        this.peersAccess.withLock {
            val peer = this.get(id)
            return if (peer != null) {
                this.remove(peer)
            } else false
        }
    }

    override fun contains(element: T): Boolean {
        this.peersAccess.withLock {
            return this.peers.contains(element)
        }
    }

    fun contains(id: UInt128): Boolean {
        this.peersAccess.withLock {
            return this.peers.any { it.kadId == id }
        }
    }

    override fun containsAll(elements: Collection<T>): Boolean {
        this.peersAccess.withLock {
            return elements.all { this.contains(it) }
        }
    }

    override fun isEmpty(): Boolean {
        return this.peers.isEmpty()
    }

    override fun iterator(): MutableIterator<T> {
        return this.peers.iterator()
    }

    override fun addAll(elements: Collection<T>): Boolean {
        this.peersAccess.withLock {
            return elements.all { this.add(it) }
        }
    }

    override fun clear() {
        this.peersAccess.withLock {
            this.peers.clear()
        }
    }

    override fun removeAll(elements: Collection<T>): Boolean {
        this.peersAccess.withLock {
            return elements.all { this.remove(it) }
        }
    }

    override fun retainAll(elements: Collection<T>): Boolean {
        TODO("Not yet implemented")
    }

    override fun closest(to: UInt128, limit: Int): Set<T> {
        this.peersAccess.withLock {
            return if (this.peers.size > 0) {
                this.peers
                    .filter { it.kadVerified && it.alive }
                    .sortedBy { it.kadId.xor(to) }
                    .take(limit)
                    .toSet()
            } else emptySet()
        }
    }

    fun update(element: T): Boolean {
        this.peersAccess.withLock {
            val peer = this[element.kadId]
            if (peer != null) {
                peer.update(element)
                return true
            }
            return false
        }
    }

    fun pushToEnd(element: T): Boolean {
        this.peersAccess.withLock {
            return if (this.peers.contains(element)) {
                this.peers.remove(element)
                this.peers.add(element)
                true
            } else false
        }
    }
}
