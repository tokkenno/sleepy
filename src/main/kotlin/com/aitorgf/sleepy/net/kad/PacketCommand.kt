package com.aitorgf.sleepy.net.kad

enum class PacketCommand(val code: Byte) {
    Empty(0x00bu.toByte()),

    // Kademlia commands

    KademliaBootstrapRequest(0x00u.toByte()),
    Kademlia2BootstrapRequest(0x01u.toByte()),
    KademliaBootstrapResponse(0x08u.toByte()),
    Kademlia2BootstrapResponse(0x09u.toByte()),

    KademliaHelloRequest(0x10u.toByte()),
    Kademlia2HelloRequest(0x11u.toByte()),
    KademliaHelloResponse(0x18u.toByte()),
    Kademlia2HelloResponse(0x19u.toByte()),

    Kademlia2HelloResponseAck(0x22u.toByte()),

    KademliaFirewalledRequest(0x50u.toByte()),
    KademliaFirewalledResponse(0x58u.toByte()),
    Kademlia2FirewalledRequest(0x53u.toByte()),
    Kademlia2FirewallUDP(0x62u.toByte()),

    KademliaCallbackRequest(0x52u.toByte()),

    KademliaRequest(0x20u.toByte()),
    Kademlia2Request(0x21u.toByte()),
    KademliaResponse(0x28u.toByte()),
    Kademlia2Response(0x29u.toByte()),

    KademliaPublishRequest(0x40u.toByte()),
    KademliaPublishResponse(0x48u.toByte()),
    Kademlia2PublishResponse(0x4Bu.toByte()),

    Kademlia2PublishResponseAck(0x4Cu.toByte()),

    KademliaSearchRequest(0x30u.toByte()),
    KademliaSearchResponse(0x38u.toByte()),
    Kademlia2SearchResponse(0x3bu.toByte()),

    KademliaSearchNotesRequest(0x32u.toByte()),
    KademliaSearchNotesResponse(0x3au.toByte()),
    Kademlia2SearchNotesRequest(0x35u.toByte()),

    Kademlia2SearchKeyRequest(0x33u.toByte()),

    Kademlia2SearchKeySourceRequest(0x34u.toByte()),

    KademliaFindBuddyRequest(0x51u.toByte()),
    KademliaFindBuddyResponse(0x5au.toByte()),

    KademliaPublishNotesRequest(0x42u.toByte()),
    KademliaPublishNotesResponse(0x4au.toByte()),
    Kademlia2PublishNotesRequest(0x45u.toByte()),

    Kademlia2PublishKeyRequest(0x43u.toByte()),

    Kademlia2PublishSourceRequest(0x44u.toByte()),

    Kademlia2Ping(0x60u.toByte()),
    Kademlia2Pong(0x61u.toByte());

    companion object {
        fun fromByte(code: Byte): PacketCommand {
            return values().firstOrNull { it.code == code } ?: Empty
        }
    }
}
