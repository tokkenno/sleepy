package com.aitorgf.sleepy.net.kad.router

import com.aitorgf.sleepy.event.EventEmitter
import com.aitorgf.sleepy.event.EventObserver
import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.net.kad.Peer
import com.aitorgf.sleepy.types.UInt128
import com.aitorgf.sleepy.types.getIntAt
import com.aitorgf.sleepy.types.getUShortAt
import io.ktor.utils.io.core.*
import java.io.InputStream

class Router<T : Peer>(
    clientId: UInt128
) : Zone<T>(localId = clientId) {
    private val peerUpdateRequestEvent = EventEmitter<Int>()
    private val peerLookupRequestEvent = EventEmitter<Int>()

    val peerUpdateRequest: EventObserver<Int>
        get() = this.peerUpdateRequestEvent

    val peerLookupRequest: EventObserver<Int>
        get() = this.peerLookupRequestEvent

    /** Get peers to do a bootstrap, with a limit of [max] peers */
    fun getBootstrap(max: Int = 20): Set<Peer> {
        return this.top(max, 5)
    }

    /** Load peers from a nodes.dat *mule/edonkey file */
    fun loadNodesDat(inputStream: InputStream, type: Class<T>) {
        val fileContent = inputStream.readBytes()
        val countItems = fileContent.getIntAt(0, ByteOrder.LITTLE_ENDIAN)
        val peerList = if (countItems == 0) {
            // Versioned file
            when (val version = fileContent.getIntAt(4, ByteOrder.LITTLE_ENDIAN)) {
                2 -> loadNodesDatV2(fileContent, type)
                else -> throw Exception("Unknown nodes.dat version <$version>")
            }
        } else loadNodesDatV0(fileContent, type)

        peerList.forEach { peer ->
            this.add(peer)
        }
    }

    companion object {
        private fun <T : Peer> loadNodesDatV0(data: ByteArray, type: Class<T>): Set<T> {
            val contactList = mutableSetOf<T>()

            val contactSize = 25
            val headerSize = 4
            for (i in 0 until data.getIntAt(0, ByteOrder.LITTLE_ENDIAN)) {
                val dataStart = headerSize + (contactSize * i)
                val contactData = data.copyOfRange(dataStart, dataStart + contactSize)

                val contact = type.getDeclaredConstructor().newInstance()
                contact.kadId = UInt128(contactData.copyOfRange(0, 16))
                contact.ipv4 = IPv4(contactData.copyOfRange(16, 20))
                contact.kadUpdPort = contactData.getUShortAt(20)
                contact.kadTcpPort = contactData.getUShortAt(22)

                contactList.add(contact)
            }

            return contactList
        }

        private fun <T : Peer> loadNodesDatV2(data: ByteArray, type: Class<T>): Set<T> {
            val contactList = mutableSetOf<T>()

            val contactSize = 34
            val headerSize = 12
            for (i in 0 until data.getIntAt(8, ByteOrder.LITTLE_ENDIAN)) {
                val dataStart = headerSize + (contactSize * i)
                val contactData = data.copyOfRange(dataStart, dataStart + contactSize)

                val contact = type.getDeclaredConstructor().newInstance()
                contact.kadId = UInt128(contactData.copyOfRange(0, 16))
                contact.ipv4 = IPv4(contactData.copyOfRange(16, 20))
                contact.kadUpdPort = contactData.getUShortAt(20)
                contact.kadTcpPort = contactData.getUShortAt(22)
                contact.kadVersion = contactData[24].toInt()
                contact.kadVerifyKey = contactData.copyOfRange(25, 33)
                contact.kadVerified = contactData[33] > 0

                contactList.add(contact)
            }

            return contactList
        }
    }
}
