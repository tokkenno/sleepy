package com.aitorgf.sleepy.net.kad.router

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.net.kad.Peer
import com.aitorgf.sleepy.types.UInt128
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.math.max
import kotlin.random.Random

open class Zone<T : Peer> : MutableCollection<T>, RouterCollection<T> {
    val localId: UInt128
    val index: UInt128
    val level: Int
    private val parent: Zone<T>?

    private val zoneAccess: Lock = ReentrantLock()

    private var bucket: KBucket<T>? = KBucket()
    private var left: Zone<T>? = null
    private var right: Zone<T>? = null

    private val bucketNotNull: KBucket<T>
        get() = if (this.isLeaf()) {
            this.bucket ?: throw Exception("The bucket must exists in leaf mode")
        } else throw Exception("The bucket is null")

    private val leftNotNull: Zone<T>
        get() = if (!this.isLeaf()) {
            this.left ?: throw Exception("The left zone must exists in branch mode")
        } else throw Exception("The left branch is null")

    private val rightNotNull: Zone<T>
        get() = if (!this.isLeaf()) {
            this.right ?: throw Exception("The right zone must exists in branch mode")
        } else throw Exception("The right branch is null")

    constructor(localId: UInt128) {
        this.localId = localId
        this.index = UInt128()
        this.level = 0
        this.parent = null
    }

    constructor(parent: Zone<T>, isRight: Boolean = false) {
        this.localId = parent.localId
        this.index = if (isRight) {
            (parent.index shl 1) + 1
        } else {
            parent.index shl 1
        }
        this.level = parent.level + 1
        this.parent = parent
    }

    val root: Zone<T>
        get() = parent?.root ?: this

    val maxDepth: Int
        get() {
            this.zoneAccess.withLock {
                return if (this.isLeaf()) {
                    0
                } else {
                    val ld = this.left?.maxDepth ?: 0
                    val lr = this.right?.maxDepth ?: 0
                    max(ld, lr) + 1
                }
            }
        }

    fun isLeaf(): Boolean {
        return this.bucket != null
    }

    fun dispose() {

    }

    override val size: Int
        get() = this.zoneAccess.withLock {
            if (this.isLeaf()) {
                this.bucket?.size ?: 0
            } else {
                (this.left?.size ?: 0) + (this.right?.size ?: 0)
            }
        }

    override fun contains(element: T): Boolean {
        this.zoneAccess.withLock {
            return if (this.isLeaf()) {
                this.bucket?.contains(element) ?: false
            } else {
                (this.left?.contains(element) ?: false) ||
                        (this.right?.contains(element) ?: false)
            }
        }
    }

    override fun containsAll(elements: Collection<T>): Boolean {
        this.zoneAccess.withLock {
            return elements.all { this.contains(it) }
        }
    }

    override fun isEmpty(): Boolean {
        this.zoneAccess.withLock {
            return if (this.isLeaf()) {
                this.bucket?.size == 0
            } else {
                (this.left?.size ?: 0) + (this.right?.size ?: 0) == 0
            }
        }
    }

    override fun iterator(): MutableIterator<T> {
        this.zoneAccess.withLock {
            if (this.isLeaf()) {
                return this.bucketNotNull.iterator()
            } else {
                TODO("Iteration over branch is not implemented")
            }
        }
    }

    override fun closest(to: UInt128, limit: Int): Set<T> {
        this.zoneAccess.withLock {
            if (this.isLeaf()) {
                return this.bucket?.closest(to, limit) ?: emptySet()
            } else {
                val rPos = to.getBit(this.level)

                val closestSide: MutableSet<T> = if (rPos) {
                    this.right?.closest(to, limit)?.toMutableSet()
                } else {
                    this.left?.closest(to, limit)?.toMutableSet()
                } ?: mutableSetOf()

                closestSide.addAll(
                    if (closestSide.size < limit) {
                        if (rPos) {
                            this.left?.closest(to, limit - closestSide.size)
                        } else {
                            this.right?.closest(to, limit - closestSide.size)
                        } ?: emptySet()
                    } else emptySet()
                )

                return closestSide
            }
        }
    }

    override fun random(): T? {
        this.zoneAccess.withLock {
            return if (this.isLeaf()) {
                this.bucket?.random()
            } else {
                if (Random.nextBoolean()) {
                    this.left?.random()
                } else {
                    this.right?.random()
                }
            }
        }
    }

    /** Get a list of peers from a random bucket, to a limit of [max] */
    private fun randomBucket(): Set<T> {
        return if (this.isLeaf()) {
            this.bucketNotNull.toSet()
        } else {
            if (Random.nextBoolean()) {
                this.leftNotNull.randomBucket()
            } else this.rightNotNull.randomBucket()
        }
    }

    operator fun get(id: UInt128): T? {
        this.zoneAccess.withLock {
            return if (this.isLeaf()) {
                this.bucketNotNull.get(id)
            } else {
                this.leftNotNull.get(id) ?: this.rightNotNull.get(id)
            }
        }
    }

    operator fun get(ip: IPv4): T? {
        this.zoneAccess.withLock {
            return if (this.isLeaf()) {
                this.bucketNotNull.get(ip)
            } else {
                this.leftNotNull.get(ip) ?: this.rightNotNull.get(ip)
            }
        }
    }

    /** Get the [max] top peers. Search to a limit of [maxDepth] on the tree */
    fun top(max: Int, maxDepth: Int = 5): Set<T> {
        this.zoneAccess.withLock {
            return if (this.isLeaf()) {
                this.bucketNotNull.take(max).toSet()
            } else if (maxDepth <= 0) {
                this.randomBucket().take(max).toSet()
            } else {
                val leftPeers = this.leftNotNull.top(max, maxDepth - 1)
                if (leftPeers.size < max) {
                    leftPeers + this.rightNotNull.top(max - leftPeers.size, maxDepth - 1)
                } else leftPeers
            }
        }
    }

    override fun add(element: T): Boolean {
        this.zoneAccess.withLock {
            return if (this.isLeaf()) {
                val bucketPtr = this.bucket ?: throw Exception("The bucket must exists in leaf mode")

                if (this.localId != element.kadId) {
                    val locPeer = bucketPtr[element.kadId]

                    if (locPeer != null) {
                        bucketPtr.update(element)
                    } else if (!bucketPtr.isFull()) {
                        bucketPtr.add(element)
                    } else if (this.canSplit()) {
                        this.split()
                        this.add(element)
                    } else {
                        throw Exception("the peer can't be added. bucket full and can't be split")
                    }
                } else {
                    throw Exception("the router can't contains itself")
                }
            } else {
                if (!(element.kadId xor this.localId).getBit(this.level)) {
                    this.left?.add(element) ?: false
                } else {
                    this.right?.add(element) ?: false
                }
            }
        }
    }

    override fun addAll(elements: Collection<T>): Boolean {
        this.zoneAccess.withLock {
            return elements.all { this.add(it) }
        }
    }

    override fun clear() {
        this.zoneAccess.withLock {
            this.bucket = KBucket()
            this.left = null
            this.right = null
        }
    }

    override fun remove(element: T): Boolean {
        return if (this.isLeaf()) {
            this.bucketNotNull.remove(element)
        } else {
            this.leftNotNull.remove(element) || this.rightNotNull.remove(element)
        }
    }

    override fun removeAll(elements: Collection<T>): Boolean {
        return elements.all(this::remove)
    }

    override fun retainAll(elements: Collection<T>): Boolean {
        var modified = false
        val it = this.iterator()
        while (it.hasNext()) {
            val peer = it.next()
            if (!elements.contains(peer)) {
                it.remove()
                modified = true
            }
        }
        return modified
    }

    /** Overridable function called when internal checks must stop */
    protected fun stopChecks() {
    }

    /** Overridable function called when internal checks must start */
    protected fun startChecks() {
    }

    /**
     * Check if this zone can be split from leaf to branch
     */
    private fun canSplit(): Boolean {
        this.zoneAccess.withLock {
            return if (this.isLeaf()) {
                if (this.level >= 127) {
                    false
                } else {
                    val bucketPtr = this.bucket ?: throw Exception("A leaf must have a bucket")
                    this.level < (MaxLevels - 1) && bucketPtr.size == KBucket.MaxBucketSize
                }
            } else false
        }
    }

    /**
     * Split a leaf to branch
     */
    private fun split() {
        this.zoneAccess.withLock {
            if (this.canSplit()) {
                val bucketPtr = this.bucket ?: throw Exception("A leaf must have a bucket")

                this.stopChecks()

                val leftZone = Zone(
                    this,
                    isRight = false
                )
                val rightZone = Zone(
                    this,
                    isRight = true
                )

                for (currPeer in bucketPtr) {
                    val distance = currPeer.kadId xor this.localId
                    if (distance.getBit(this.level)) {
                        rightZone.add(currPeer)
                    } else {
                        leftZone.add(currPeer)
                    }
                }

                leftZone.startChecks()
                rightZone.startChecks()

                this.left = leftZone
                this.right = rightZone
                this.bucket = null
            } else {
                throw Exception("can't split")
            }
        }
    }

    override fun toString(): String {
        val sb = StringBuilder()
        for (i in 0 until this.level) {
            sb.append("\t")
        }
        val tabs = sb.toString()

        return if (this.isLeaf()) {
            val leafSb = StringBuilder()
            leafSb.append("$tabs${index.toString(2)} ($level) <$size>")
            this.bucket?.forEach {
                leafSb.append("\n$tabs\t${it.kadId.toString(2)}")
            }
            leafSb.toString()
        } else {
            if (this.level == 0) {
                "Zone ($localId) <$size>\n$left\n$right"
            } else {
                val zoneSb = StringBuilder()
                for (i in 0 until this.level) {
                    zoneSb.append("\t")
                }
                zoneSb.append("${index.toString(2)} ($level) <$size>\n$left\n$right")
                zoneSb.toString()
            }
        }
    }

    companion object {
        private const val MaxLevels = 6
    }
}

