package com.aitorgf.sleepy.net.kad

interface RequestHandler {
    fun canHandle(command: PacketCommand): Boolean

    fun handle(reader: KadPacketReader): Unit
}
