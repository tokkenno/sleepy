package com.aitorgf.sleepy.net.kad.router

import com.aitorgf.sleepy.net.kad.Peer
import com.aitorgf.sleepy.types.UInt128

interface RouterCollection<T : Peer> {
    /** Get random peer from collection, or null if collection empty */
    fun random(): T?

    /** Get the closest peer by ID distance [to] other ID till [limit] */
    fun closest(to: UInt128, limit: Int): Set<T>
}
