package com.aitorgf.sleepy.net.kad

import com.aitorgf.sleepy.net.common.UDPPacketBuilder

class KadPacketBuilder : UDPPacketBuilder() {
    var protocol: PacketProtocol
        set(value) {
            this.body[0] = value.code
        }
        get() = throw UnsupportedOperationException()

    var command: PacketCommand
        set(value) {
            if (this.protocol == PacketProtocol.EdonkeyServerUdp ||
                this.protocol == PacketProtocol.KadUdp
            ) {
                this.body[1] = value.code
            } else {
                this.body[5] = value.code
            }
        }
        get() = throw UnsupportedOperationException()
}
