package com.aitorgf.sleepy.net.kad

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.net.common.UDPPacket
import com.aitorgf.sleepy.net.common.UDPPacketReader
import com.aitorgf.sleepy.types.UInt128
import java.nio.ByteBuffer
import java.util.zip.DataFormatException

class KadPacketReader(packet: UDPPacket) : UDPPacketReader(packet) {
    val protocol: PacketProtocol
        get() {
            return if (this.packet.data.isNotEmpty()) PacketProtocol.fromByte(this.packet.data[0]) else PacketProtocol.Empty
        }

    val command: PacketCommand
        get() {
            return when (this.protocol) {
                PacketProtocol.EdonkeyServerUdp,
                PacketProtocol.KadUdp -> if (this.packet.data.size >= 2) PacketCommand.fromByte(this.packet.data[1]) else PacketCommand.Empty
                else -> if (this.packet.data.size >= 6) PacketCommand.fromByte(this.packet.data[5]) else PacketCommand.Empty
            }
        }

    fun decompress(): UDPPacket {
        if (this.isCompressed) {
            TODO("Not support compression yet")
        } else return this.packet
    }

    val isCompressed: Boolean
        get() = if (this.packet.data.isNotEmpty()) {
            this.packet.data[0] == PacketProtocol.KadCompressedUdp.code
        } else false

    fun readPeer(position: Int = -1): Peer {
        return KadPeer(
            kadId = UInt128(this.readBytes(16, position = position)),
            ipv4 = IPv4(this.readBytes(4)),
            kadUpdPort = this.readUShort(),
            kadTcpPort = this.readUShort(),
            kadVersion = this.readByte().toInt(),
        )
    }
}
