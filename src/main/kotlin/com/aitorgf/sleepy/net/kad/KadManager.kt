package com.aitorgf.sleepy.net.kad

import com.aitorgf.sleepy.net.common.UDPPacket
import com.aitorgf.sleepy.net.kad.router.Router
import com.aitorgf.sleepy.service.Service
import com.aitorgf.sleepy.service.ServiceStatus
import com.aitorgf.sleepy.types.UInt128
import java.nio.ByteBuffer
import java.time.LocalDateTime
import java.util.*

class KadManager : Service {
    val router: Router<Peer> = Router(UInt128.random())

    val packetHandlers: List<RequestHandler> = listOf(
        KademliaRequestHandler()
    )

    override var status: ServiceStatus = ServiceStatus.New
        private set

    override fun init() {
        this.status = ServiceStatus.Initialized
    }

    override fun start() {
        this.status = ServiceStatus.Started
    }

    override fun stop() {
        this.status = ServiceStatus.Stopped
    }

    val connected: Boolean = false

    private fun processPacket(packet: UDPPacket) {
        if (!this.connected) {
            return
        }
        var packetReader = KadPacketReader(packet)
        if (packetReader.isCompressed) {
            packetReader = KadPacketReader(packetReader.decompress())
        }

        if (packetReader.protocol != PacketProtocol.KadUdp) {
            println("Received packet with unknown protocol ${packetReader.protocol}") // FIXME: Remove
            throw Exception("Invalid packet protocol")
        }

        // Update the peer's information
        val existingContact = this.router[packetReader.senderIPv4]
        if (existingContact != null) {
            existingContact.kadLastResponse = LocalDateTime.now()
            existingContact.kadConnected = true
            existingContact.kadVerified = true
        }

        for (handler in this.packetHandlers) {
            if (handler.canHandle(packetReader.command)) {
                handler.handle(packetReader)
                break
            }
        }
    }
}
