package com.aitorgf.sleepy.net.kad

enum class PacketProtocol(val code: Byte) {
    Empty(0x00u.toByte()),
    EdonkeyServerUdp(0xE3u.toByte()),
    EdonkeyPeerUdp(0xC5u.toByte()),
    KadUdp(0xE4u.toByte()),
    KadCompressedUdp(0xE5u.toByte());

    companion object {
        fun fromByte(code: Byte): PacketProtocol {
            return values().firstOrNull { it.code == code } ?: Empty
        }
    }
}
