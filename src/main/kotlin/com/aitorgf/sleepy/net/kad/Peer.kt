package com.aitorgf.sleepy.net.kad

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.net.Peer
import com.aitorgf.sleepy.types.UInt128
import java.time.LocalDateTime

interface Peer : com.aitorgf.sleepy.net.Peer {
    var kadId: UInt128

    var alive: Boolean

    override var ipv4: IPv4

    var kadUpdPort: UShort

    var kadTcpPort: UShort

    var kadVersion: Int

    var kadVerified: Boolean

    var kadVerifyKey: ByteArray?

    var kadLastResponse: LocalDateTime

    var kadConnected: Boolean

    override fun update(other: Peer): Boolean
}
