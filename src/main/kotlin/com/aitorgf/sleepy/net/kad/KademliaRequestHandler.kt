package com.aitorgf.sleepy.net.kad

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.types.UInt128
import java.util.*

class KademliaRequestHandler : RequestHandler {
    private val canHandleCommands = listOf(
        PacketCommand.KademliaBootstrapRequest,
        PacketCommand.KademliaBootstrapResponse,
        PacketCommand.KademliaHelloRequest,
        PacketCommand.KademliaHelloResponse,
        PacketCommand.KademliaRequest,
        PacketCommand.KademliaResponse,
        PacketCommand.KademliaPublishRequest,
        PacketCommand.KademliaPublishResponse,
        PacketCommand.KademliaSearchNotesRequest,
        PacketCommand.KademliaSearchNotesResponse,
        PacketCommand.KademliaPublishNotesRequest,
        PacketCommand.KademliaPublishNotesResponse,
        PacketCommand.KademliaSearchRequest,
        PacketCommand.KademliaSearchResponse,
        PacketCommand.KademliaFirewalledRequest,
        PacketCommand.KademliaFirewalledResponse
    )

    override fun canHandle(command: PacketCommand): Boolean {
        return this.canHandleCommands.contains(command)
    }

    override fun handle(reader: KadPacketReader) {
        when (reader.command) {
            PacketCommand.KademliaBootstrapRequest -> this.bootstrapRequest(reader)
            PacketCommand.KademliaBootstrapResponse -> this.bootstrapResponse(reader)
            PacketCommand.KademliaHelloRequest -> this.helloRequest(reader)
            PacketCommand.KademliaHelloResponse -> this.helloResponse(reader)
            PacketCommand.KademliaRequest -> this.request(reader)
            PacketCommand.KademliaResponse -> this.response(reader)
            PacketCommand.KademliaPublishRequest -> this.publishRequest(reader)
            PacketCommand.KademliaPublishResponse -> this.publishResponse(reader)
            PacketCommand.KademliaSearchNotesRequest -> this.searchNotesRequest(reader)
            PacketCommand.KademliaSearchNotesResponse -> this.searchNotesResponse(reader)
            PacketCommand.KademliaPublishNotesRequest -> this.publishNotesRequest(reader)
            PacketCommand.KademliaPublishNotesResponse -> this.publishNotesResponse(reader)
            PacketCommand.KademliaSearchRequest -> this.searchRequest(reader)
            PacketCommand.KademliaSearchResponse -> this.searchResponse(reader)
            PacketCommand.KademliaFirewalledRequest -> this.firewalledRequest(reader)
            PacketCommand.KademliaFirewalledResponse -> this.firewalledResponse(reader)
            else -> throw IllegalStateException("Unknown command: ${reader.command}")
        }
    }

    private fun bootstrapRequest(reader: KadPacketReader) {
        val clientId = UInt128(reader.readBytes(16, 3))
        val clientAddress = IPv4(reader.readBytes(4))
        val udpPort = reader.readShort()
        // bootStrap.processBootStrapReq(clientId, clientAddress, udpPort)
        TODO("Not yet implemented")
    }

    private fun bootstrapResponse(reader: KadPacketReader) {
        val peerCount = reader.readShort(3)
        val peerList = mutableListOf<Peer>()
        for (i in 0 until peerCount) {
            peerList.add(reader.readPeer())
        }
        //bootStrap.processBootStrapRes(contact_list)
        TODO("Not yet implemented")
    }

    private fun helloRequest(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun helloResponse(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun request(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun response(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun publishRequest(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun publishResponse(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun searchNotesRequest(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun searchNotesResponse(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun publishNotesRequest(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun publishNotesResponse(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }


    private fun searchRequest(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun searchResponse(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun firewalledRequest(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }

    private fun firewalledResponse(reader: KadPacketReader) {
        TODO("Not yet implemented")
    }
}
