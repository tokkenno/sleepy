package com.aitorgf.sleepy.net.kad

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.types.UInt128
import java.time.LocalDateTime

data class KadPeer(
    override var kadId: UInt128,
    override var alive: Boolean = false,
    override var ipv4: IPv4,
    override var kadUpdPort: UShort,
    override var kadTcpPort: UShort,
    override var kadVersion: Int,
    override var kadVerified: Boolean = false,
    override var kadVerifyKey: ByteArray? = null,
    override var kadLastResponse: LocalDateTime = LocalDateTime.MIN,
    override var kadConnected: Boolean = false
): Peer {
    override fun update(other: com.aitorgf.sleepy.net.Peer): Boolean {
        TODO("Not yet implemented")
    }
}
