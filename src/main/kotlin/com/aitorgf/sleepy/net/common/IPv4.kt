package com.aitorgf.sleepy.net.common

class IPv4(
    bytes: ByteArray = ByteArray(4)
) {
    val bytes: ByteArray = bytes.copyOf(4)
    constructor(a0: Int, a1: Int, a2: Int, a3: Int) : this(
        byteArrayOf(
            a0.toByte(),
            a1.toByte(),
            a2.toByte(),
            a3.toByte()
        )
    )

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        else if (this === other) return true
        else if (other !is IPv4) return false
        return this.bytes.contentEquals(other.bytes)
    }

    override fun hashCode(): Int {
        return this.bytes.contentHashCode()
    }

    override fun toString(): String {
        return "${this.bytes[0].toUByte()}.${this.bytes[1].toUByte()}.${this.bytes[2].toUByte()}.${this.bytes[3].toUByte()}"
    }

    companion object {
        fun random(): IPv4 {
            return IPv4(
                (Math.random() * 255).toInt(),
                (Math.random() * 255).toInt(),
                (Math.random() * 255).toInt(),
                (Math.random() * 255).toInt()
            )
        }
    }
}
