package com.aitorgf.sleepy.net.common

import java.net.InetAddress
import java.net.InetSocketAddress

data class UDPPacket(
    val data: ByteArray,

    val sender: InetSocketAddress
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UDPPacket

        if (!data.contentEquals(other.data)) return false
        if (sender != other.sender) return false

        return true
    }

    override fun hashCode(): Int {
        var result = data.contentHashCode()
        result = 31 * result + sender.hashCode()
        return result
    }
}
