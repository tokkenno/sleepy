package com.aitorgf.sleepy.net.common

import java.nio.ByteBuffer
import java.nio.ByteOrder

open class UDPPacketReader(val packet: UDPPacket) {
    val senderIPv4: IPv4
        get() = IPv4(this.packet.sender.address.address)

    val senderPort: Int
        get() = this.packet.sender.port

    private var seek: Int = 0

    fun readByte(): Byte {
        return this.packet.data[this.seek++]
    }

    fun readBytes(size: Int = -1, position: Int = -1): ByteArray {
        val start = if (position == -1) this.seek else position
        val end = if (size == -1) this.packet.data.size - position else Math.min(
            position + size,
            this.packet.data.size - position
        )
        this.seek = end
        return this.packet.data.copyOfRange(start, end)
    }

    fun readShort(position: Int = -1): Short {
        return ByteBuffer.wrap(this.readBytes(2, position = position)).short
    }

    fun readUShort(position: Int = -1): UShort {
        return this.readShort(position).toUShort()
    }
}
