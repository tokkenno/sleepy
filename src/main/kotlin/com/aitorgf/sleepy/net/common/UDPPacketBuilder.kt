package com.aitorgf.sleepy.net.common

import io.ktor.utils.io.core.*
import java.net.InetSocketAddress

open class UDPPacketBuilder {
    var body = ByteArray(0)
    var address: InetSocketAddress? = null

    fun write(data: ByteArray, position: Int = -1) {
        if (position >= 0) {
            if (position + data.size > this.body.size) {
                val newContainer = ByteArray(position + data.size)
                this.body.copyInto(newContainer)
                this.body = newContainer
            }
            data.copyInto(this.body, position)
        } else {
            val newContainer = ByteArray(this.body.size + data.size)
            this.body.copyInto(newContainer)
            data.copyInto(newContainer, this.body.size)
            this.body = newContainer
        }
    }

    fun write(byte: Byte, position: Int = -1) {
        write(byteArrayOf(byte), position)
    }

    fun write(short: Short, position: Int = -1, endian: ByteOrder = ByteOrder.BIG_ENDIAN) {
        write(
            if (endian == ByteOrder.BIG_ENDIAN) {
                byteArrayOf((short.toInt() shr 8).toByte(), short.toByte())
            } else {
                byteArrayOf(short.toByte(), (short.toInt() shr 8).toByte())
            }, position
        )
    }

    fun write(int: Int, position: Int = -1, endian: ByteOrder = ByteOrder.BIG_ENDIAN) {
        write(
            if (endian == ByteOrder.BIG_ENDIAN) {
                byteArrayOf((int shr 24).toByte(), (int shr 16).toByte(), (int shr 8).toByte(), int.toByte())
            } else {
                byteArrayOf(int.toByte(), (int shr 8).toByte(), (int shr 16).toByte(), (int shr 24).toByte())
            }, position
        )
    }

    fun clear() {
        this.body = ByteArray(0)
    }


    fun build(): UDPPacket {
        val addr = this.address ?: throw IllegalStateException("Address not set")
        return UDPPacket(this.body, addr)
    }
}
