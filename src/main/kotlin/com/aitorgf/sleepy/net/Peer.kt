package com.aitorgf.sleepy.net

import com.aitorgf.sleepy.net.common.IPv4

interface Peer {
    val ipv4: IPv4

    /** Update peer information with other instance of peer */
    fun update(other: Peer): Boolean
}
