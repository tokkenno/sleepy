package com.aitorgf.sleepy.service

enum class ServiceStatus {
    New,
    Initialized,
    Started,
    Stopped,
    Error
}
