package com.aitorgf.sleepy.service

interface Service {
    val status: ServiceStatus

    /** Called when service created to initialize the internal logic */
    fun init()

    /** Start the service */
    fun start()

    /** Stop the service */
    fun stop()
}
