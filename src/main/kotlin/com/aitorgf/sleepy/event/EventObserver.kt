package com.aitorgf.sleepy.event

interface EventObserver<T> {
    fun subscribe(next: (value: T) -> Unit): EventSubscription<T>

    fun unsubscribe(next: (value: T) -> Unit): Boolean
}
