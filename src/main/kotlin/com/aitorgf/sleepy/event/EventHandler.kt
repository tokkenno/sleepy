package com.aitorgf.sleepy.event

data class EventHandler<T>(
    val next: (next: T) -> Unit,
    var catch: ((e: Exception) -> Unit)?
)
