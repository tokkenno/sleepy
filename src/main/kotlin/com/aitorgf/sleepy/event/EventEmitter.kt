package com.aitorgf.sleepy.event

import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.thread
import kotlin.concurrent.withLock

class EventEmitter<T>: EventObserver<T> {
    private val handlers = mutableSetOf<EventHandler<T>>()
    private val eventAccess: Lock = ReentrantLock()

    fun next(value: T) {
        thread(start = true) {
            this.nextSync(value)
        }
    }

    fun nextSync(value: T) {
        this.eventAccess.withLock {
            this.handlers.forEach { handler ->
                try {
                    handler.next(value)
                } catch (e: Exception) {
                    val catchFunc = handler.catch
                    if (catchFunc != null) {
                        catchFunc(e)
                    } else {
                        throw e
                    }
                }
            }
        }
    }

    override fun subscribe(next: (value: T) -> Unit): EventSubscription<T> {
        val handler = EventHandler(next = next, catch = null)
        val subscription = EventSubscription(this, handler)

        this.eventAccess.withLock {
            this.handlers.add(handler)
        }

        return subscription
    }

    internal fun unsubscribe(handler: EventHandler<T>) {
        this.eventAccess.withLock {
            this.handlers.remove(handler)
        }
    }

    override fun unsubscribe(next: (value: T) -> Unit): Boolean {
        this.eventAccess.withLock {
            return this.handlers.removeIf { it.next == next }
        }
    }
}
