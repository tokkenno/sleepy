package com.aitorgf.sleepy.event

class EventSubscription<T>(
    private val emitter: EventEmitter<T>,
    private val handler: EventHandler<T>
) {
    fun catch(errCb: (e: Exception) -> Unit): EventSubscription<T> {
        handler.catch = errCb
        return this
    }

    fun unsubscribe() {
        this.emitter.unsubscribe(this.handler)
    }
}
