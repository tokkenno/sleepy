package com.aitorgf.sleepy.types

import io.ktor.utils.io.core.*
import kotlin.experimental.inv
import kotlin.experimental.or
import kotlin.experimental.xor
import kotlin.math.min
import kotlin.random.Random

class UInt128 : Number, Comparable<UInt128> {
    private val bytes = ByteArray(SIZE_BYTES)

    constructor() : this(0L, 0L)

    constructor(value: Byte) : this(
        if (value >= 0) {
            0L
        } else -1L,
        if (value >= 0) {
            value.toLong() and 0x00000000000000FFL
        } else value.toLong() and 0x00000000000000FFL or 0xFFFFFFFFFFFFFF00uL.toLong()
    )

    constructor(value: Short) : this(
        if (value >= 0) {
            0L
        } else -1L,
        if (value >= 0) {
            value.toLong() and 0x000000000000FFFFL
        } else value.toLong() and 0x000000000000FFFFL or 0xFFFFFFFFFFFF0000uL.toLong()
    )

    constructor(value: Int) : this(
        if (value >= 0) {
            0L
        } else -1L,
        if (value >= 0) {
            value.toLong() and 0x00000000FFFFFFFFL
        } else value.toLong() and 0x00000000FFFFFFFFL or 0xFFFFFFFF00000000uL.toLong()
    )

    constructor(value: Long) : this(
        if (value >= 0) {
            0L
        } else -1L,
        value
    )

    constructor(upValue: Long, downValue: Long) {
        this.bytes.setLongAt(0, upValue)
        this.bytes.setLongAt(8, downValue)
    }

    constructor(value: ByteArray) {
        for (i in 0 until min(SIZE_BYTES, value.size)) {
            this.bytes[i] = value[i]
        }
    }

    fun toByteArray(): ByteArray {
        return this.bytes.copyOf()
    }

    override fun toString(): String {
        return this.bytes.toHex()
    }

    fun toString(radix: Int): String {
        return when (radix) {
            16 -> this.bytes.toHex()
            2 -> this.bytes.toBin()
            else -> throw Exception("Radix not supported")
        }
    }

    fun toBin(): String {
        return this.bytes.toBin()
    }

    /**
     * {@inheritDoc}
     */
    override operator fun compareTo(other: UInt128): Int {
        val lower63BitMask = 0x7FFFFFFFFFFFFFFFL
        val up = this.bytes.getLongAt(0)
        val low = this.bytes.getLongAt(8)
        val otherUp = other.bytes.getLongAt(0)
        val otherLow = other.bytes.getLongAt(8)

        if (up != otherUp) {
            if (up < 0 && otherUp > 0) {
                return 1
            }
            if (up > 0 && otherUp < 0) {
                return -1
            }
            return if (up and lower63BitMask
                > otherUp and lower63BitMask
            ) {
                1
            } else -1
        } else if (low != otherLow) {
            if (low < 0 && otherLow > 0) {
                return 1
            }
            if (low > 0 && otherLow < 0) {
                return -1
            }
            return if (low and lower63BitMask
                > otherLow and lower63BitMask
            ) {
                1
            } else -1
        }
        return 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        if (other::class != UInt128::class) return false
        other as UInt128
        return this.bytes.contentEquals(other.bytes)
    }

    override fun hashCode(): Int {
        return this.bytes.contentHashCode()
    }

    fun clone(): UInt128 {
        return UInt128(this.bytes.copyOf())
    }

    fun getBit(position: Int, startFromLeft: Boolean = false): Boolean {
        val dataIndex = if (startFromLeft) {
            position / 8
        } else 15 - (position / 8)
        return if (startFromLeft) {
            this.bytes[dataIndex].toInt() shr (7 - (position % 8)) and 0x1 == 0x1
        } else this.bytes[dataIndex].toInt() shr (position % 8) and 0x1 == 0x1
    }

    fun setBit(position: Int, value: Boolean, startFromLeft: Boolean = false): UInt128 {
        val copyBytes = this.bytes.copyOf()

        val dataIndex = if (startFromLeft) {
            position / 8
        } else 15 - (position / 8)

        if (!value) {
            copyBytes[dataIndex] = copyBytes[dataIndex].inv()
        }

        val mask = if (startFromLeft) {
            (0x1 shl (7 - (position % 8))).toByte()
        } else (0x1 shl (position % 8)).toByte()

        copyBytes[dataIndex] = copyBytes[dataIndex] or mask

        if (!value) {
            copyBytes[dataIndex] = copyBytes[dataIndex].inv()
        }

        return UInt128(copyBytes)
    }

    override fun toByte(): Byte {
        return if (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN) {
            this.bytes[SIZE_BYTES - 1]
        } else {
            this.bytes[0]
        }
    }

    override fun toChar(): Char {
        return this.toInt().toChar()
    }

    override fun toDouble(): Double {
        return this.toLong().toDouble()
    }

    override fun toFloat(): Float {
        return this.toInt().toFloat()
    }

    override fun toInt(): Int {
        var result = 0
        for ((ind, byteInd) in (SIZE_BYTES - 1 downTo SIZE_BYTES - 4).withIndex()) {
            result = result or (this.bytes[byteInd].toInt() and 0xFF shl (ind * 8))
        }
        return result
    }

    override fun toLong(): Long {
        var result = 0L
        for ((ind, byteInd) in (SIZE_BYTES - 1 downTo SIZE_BYTES - 8).withIndex()) {
            result = result or (this.bytes[byteInd].toLong() and 0xFF shl (ind * 8))
        }
        return result
    }

    override fun toShort(): Short {
        var result = 0
        for ((ind, byteInd) in (SIZE_BYTES - 1 downTo SIZE_BYTES - 2).withIndex()) {
            result = result or (this.bytes[byteInd].toInt() and 0xFF shl (ind * 8))
        }
        return result.toShort()
    }

    fun toBitSet(): BitSet {
        return BitSet(this.bytes)
    }

    infix fun xor(other: UInt128): UInt128 {
        return UInt128(this.bytes.mapIndexed { index, byte -> byte xor other.bytes[index] }.toByteArray())
    }

    infix fun shl(i: Int): UInt128 {
        return if (i == 0) {
            this.clone()
        } else if (i > 128) {
            UInt128()
        } else {
            val tmp = this.bytes.getLongAt(0) shr (64 - i)
            UInt128(
                this.bytes.getLongAt(0) shl i,
                this.bytes.getLongAt(8) shl i or tmp
            )
        }
    }

    infix fun shr(i: Int): UInt128 {
        return if (i == 0) {
            this.clone()
        } else if (i > 128) {
            UInt128()
        } else {
            val tmp = this.bytes.getLongAt(0) shl (64 - i)
            UInt128(
                this.bytes.getLongAt(0) shr i,
                this.bytes.getLongAt(8) shr i or tmp
            )
        }
    }

    operator fun plus(increment: UInt128): UInt128 {
        val carry = this.bytes.getLongAt(8)
        val lo = this.bytes.getLongAt(8) + increment.bytes.getLongAt(8)
        var hi = this.bytes.getLongAt(0) + increment.bytes.getLongAt(0)
        if (lo < carry) {
            hi++
        }
        return UInt128(hi, lo)
    }

    operator fun plus(increment: Int): UInt128 {
        return this.plus(UInt128(increment))
    }

    companion object {
        private const val SIZE_BITS: Int = 128
        const val SIZE_BYTES: Int = SIZE_BITS / Byte.SIZE_BITS

        fun random(): UInt128 {
            return UInt128(Random.Default.nextBytes(SIZE_BYTES))
        }
    }
}
