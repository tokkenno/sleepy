package com.aitorgf.sleepy.net.kad.router

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.net.kad.Peer
import com.aitorgf.sleepy.types.UInt128
import java.time.LocalDateTime

data class TestPeer(
    override var ipv4: IPv4,
    override var kadId: UInt128,
    override var alive: Boolean = false,
    override var kadVerified: Boolean = false,
    override var kadUpdPort: UShort = 0u,
    override var kadTcpPort: UShort = 0u,
    override var kadVersion: Int = 0,
    override var kadVerifyKey: ByteArray? = null,
    override var kadLastResponse: LocalDateTime = LocalDateTime.now(),
    override var kadConnected: Boolean = false,
) : Peer {
    constructor() : this(ipv4 = IPv4(), kadId = UInt128.random())

    override fun update(other: com.aitorgf.sleepy.net.Peer): Boolean {
        this.ipv4 = other.ipv4
        if (other is Peer) {
            this.alive = other.alive
            this.kadVerified = other.kadVerified
            this.kadUpdPort = other.kadUpdPort
            this.kadTcpPort = other.kadTcpPort
            this.kadVersion = other.kadVersion
            this.kadVerifyKey = other.kadVerifyKey
        }
        return true
    }
}
