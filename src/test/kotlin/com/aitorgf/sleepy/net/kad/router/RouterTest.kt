package com.aitorgf.sleepy.net.kad.router

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.types.UInt128
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class RouterTest {
    @Test
    fun `Test get bootstrap peers`() {
        val router = Router<TestPeer>(UInt128())

        for (i in 1 until 20) {
            router.add(TestPeer(kadId = UInt128(i), ipv4 = IPv4.random()))
        }

        val bootstrap = router.getBootstrap(10)
        assertEquals(10, bootstrap.size)
    }

    @Test
    fun `Load nodes dat v1 file`() {
        val router = Router<TestPeer>(UInt128.random())

        val resource = RouterTest::class.java.getResource("/nodes.dat")
        assertNotNull(resource)

        assertEquals(0, router.size)
        router.loadNodesDat(resource.openStream(), TestPeer::class.java)
        assertEquals(2, router.size)
    }

    @Test
    fun `Load nodes dat v2 file`() {
        val router = Router<TestPeer>(UInt128.random())

        val resourceV2 = RouterTest::class.java.getResource("/nodes_v2.dat")
        assertNotNull(resourceV2)

        assertEquals(0, router.size)
        router.loadNodesDat(resourceV2.openStream(), TestPeer::class.java)
        assertEquals(197, router.size)
    }
}
