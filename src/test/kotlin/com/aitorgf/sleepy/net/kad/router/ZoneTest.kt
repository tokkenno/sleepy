package com.aitorgf.sleepy.net.kad.router

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.types.UInt128
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ZoneTest {
    @Test
    fun `Test collection`() {
        val zone = Zone<TestPeer>(UInt128.random())
        val numPeers = KBucket.MaxBucketSize * 3 + 1

        for (i in 0 until numPeers) {
            zone.add(TestPeer(ipv4 = IPv4.random(), kadId = UInt128.random()))
        }

        assertEquals(numPeers, zone.size)
    }
}
