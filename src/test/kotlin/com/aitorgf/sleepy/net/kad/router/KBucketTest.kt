package com.aitorgf.sleepy.net.kad.router

import com.aitorgf.sleepy.net.common.IPv4
import com.aitorgf.sleepy.types.UInt128
import org.junit.jupiter.api.Test
import kotlin.concurrent.thread
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class KBucketTest {
    @Test
    fun `Add peer`() {
        val peer = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)

        val bucket = KBucket<TestPeer>()
        bucket.add(peer)

        assertEquals(1, bucket.size)
        assertEquals(peer, bucket[peer.kadId])
    }

    @Test
    fun `Add multiple peers`() {
        val peer1 = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)
        val peer2 = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)
        val peer3 = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)

        val bucket = KBucket<TestPeer>()
        bucket.add(peer1)
        bucket.add(peer2)
        bucket.add(peer3)

        assertEquals(3, bucket.size)
        assertEquals(peer2, bucket[peer2.kadId])
    }

    @Test
    fun `Count peers`() {
        val peer1 = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)
        val peer2 = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)

        val bucket = KBucket<TestPeer>()

        bucket.add(peer1)
        assertEquals(1, bucket.size)

        bucket.add(peer2)
        assertEquals(2, bucket.size)
    }

    @Test
    fun `Get peer by id`() {
        val peerId = UInt128.random()
        val peer = TestPeer(ipv4 = IPv4(), kadId = peerId, alive = false, kadVerified = false)

        val bucket = KBucket<TestPeer>()
        bucket.add(peer)

        assertEquals(1, bucket.size)
        assertEquals(peer, bucket[peerId])
    }

    @Test
    fun `Get peer by IPv4 address`() {
        val address = IPv4(192, 168, 1, 1)
        val peer = TestPeer(ipv4 = address, kadId = UInt128.random(), alive = false, kadVerified = false)

        val bucket = KBucket<TestPeer>()
        bucket.add(peer)

        assertEquals(1, bucket.size)
        assertEquals(peer, bucket[address])
    }

    @Test
    fun `Get peer list`() {
        val peer1 = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)
        val peer2 = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)
        val peer3 = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)

        val bucket = KBucket<TestPeer>()
        bucket.add(peer1)
        bucket.add(peer2)

        val peerList = bucket.toList()
        assertEquals(2, peerList.size)
        assertEquals(2, bucket.size)

        bucket.add(peer3)

        assertEquals(2, peerList.size)
        assertEquals(3, bucket.size)
    }

    @Test
    fun `Remove peer`() {
        val peer = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)

        val bucket = KBucket<TestPeer>()
        bucket.add(peer)

        assertEquals(1, bucket.size)

        bucket.remove(peer)

        assertTrue(bucket.isEmpty())
    }

    @Test
    fun `Remove peer by id`() {
        val peerId = UInt128.random()
        val peer = TestPeer(ipv4 = IPv4(), kadId = peerId, alive = false, kadVerified = false)

        val bucket = KBucket<TestPeer>()
        bucket.add(peer)

        assertEquals(1, bucket.size)

        bucket.remove(peerId)

        assertTrue(bucket.isEmpty())
    }

    @Test
    fun `Fill bucket`() {
        val bucket = KBucket<TestPeer>()

        for (i in 0 until KBucket.MaxBucketSize) {
            bucket.add(TestPeer(ipv4 = IPv4.random(), kadId = UInt128.random(), alive = false, kadVerified = false))
        }

        assertTrue(bucket.isFull())
    }

    @Test
    fun `Multithreading add peer`() {
        val bucket = KBucket<TestPeer>()

        val t1 = thread(start = true) {
            for (i in 0 until 3) {
                bucket
                    .add(TestPeer(ipv4 = IPv4.random(), kadId = UInt128.random(), alive = false, kadVerified = false))
            }
        }

        val t2 = thread(start = true) {
            for (i in 0 until 3) {
                bucket
                    .add(TestPeer(ipv4 = IPv4.random(), kadId = UInt128.random(), alive = false, kadVerified = false))
            }
        }

        val t3 = thread(start = true) {
            for (i in 0 until 3) {
                bucket
                    .add(TestPeer(ipv4 = IPv4.random(), kadId = UInt128.random(), alive = false, kadVerified = false))
            }
        }

        t1.join()
        t2.join()
        t3.join()
        assertEquals(9, bucket.size)
    }

    @Test
    fun `Update a peer`() {
        val bucket = KBucket<TestPeer>()

        val peerId = UInt128.random()

        val originalPeer = TestPeer(ipv4 = IPv4(192, 168, 1, 1), kadId = peerId, alive = false, kadVerified = false)
        bucket.add(originalPeer)
        assertEquals(1, bucket.size)

        val modPeer = TestPeer(ipv4 = IPv4(192, 168, 1, 2), kadId = peerId, alive = true, kadVerified = true)
        bucket.update(modPeer)
        assertEquals(1, bucket.size)

        assertEquals(true, originalPeer.kadVerified)
    }

    @Test
    fun `Push peer to end`() {
        val bucket = KBucket<TestPeer>()
        val peer = TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false)

        bucket.add(peer)
        bucket.add(TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false))
        bucket.add(TestPeer(ipv4 = IPv4(), kadId = UInt128.random(), alive = false, kadVerified = false))
        assertEquals(3, bucket.size)
        assertEquals(peer.kadId, bucket.toSet().first().kadId)

        bucket.pushToEnd(peer)
        assertEquals(peer.kadId, bucket.toSet().last().kadId)
    }
}
