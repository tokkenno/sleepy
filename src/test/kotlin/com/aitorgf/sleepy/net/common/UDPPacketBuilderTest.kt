package com.aitorgf.sleepy.net.common

import io.ktor.utils.io.core.*
import kotlin.test.Test
import kotlin.test.assertEquals

class UDPPacketBuilderTest {
    @Test
    fun `Test write bytes`() {
        val builder = UDPPacketBuilder()

        builder.write(byteArrayOf(1, 2, 3, 4, 5, 6, 7, 8))
        assertEquals(8, builder.body.size)

        builder.write(byteArrayOf(1, 2, 3, 4, 5, 6, 7, 8))
        assertEquals(16, builder.body.size)

        builder.write(byteArrayOf(1, 2, 3, 4, 5, 6, 7, 8), 12)
        assertEquals(20, builder.body.size)
    }

    @Test
    fun `Test write short`() {
        val builder = UDPPacketBuilder()

        builder.write(short = 32452, endian = ByteOrder.LITTLE_ENDIAN)
        builder.write(short = 32452, endian = ByteOrder.BIG_ENDIAN)

        assertEquals(4, builder.body.size)
        assertEquals(-0x3c, builder.body[0])
        assertEquals(0x7e, builder.body[1])
        assertEquals(0x7e, builder.body[2])
        assertEquals(-0x3c, builder.body[3])
    }
}
