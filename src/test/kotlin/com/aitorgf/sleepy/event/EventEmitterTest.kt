package com.aitorgf.sleepy.event

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals

class EventEmitterTest {
    @Test
    fun `Test sync event emission`() {
        val eventEmitter = EventEmitter<Boolean>()

        var testValue = false
        eventEmitter.subscribe {
            testValue = it
        }

        assertEquals(false, testValue)
        eventEmitter.nextSync(true)
        assertEquals(true, testValue)
    }

    @Test
    fun `Test async event emission`() {
        val eventEmitter = EventEmitter<Boolean>()

        var testValue = false
        eventEmitter.subscribe {
            testValue = it
        }

        assertEquals(false, testValue)
        eventEmitter.next(true)
        assertEquals(false, testValue)
        Thread.sleep(10)
        assertEquals(true, testValue)
    }

    @Test
    fun `Test unsubscribe`() {
        val eventEmitter = EventEmitter<Boolean>()

        var testValue = false
        val subscription = eventEmitter.subscribe {
            testValue = it
        }

        assertEquals(false, testValue)
        eventEmitter.nextSync(true)
        assertEquals(true, testValue)
        subscription.unsubscribe()
        eventEmitter.nextSync(false)
        assertEquals(true, testValue)
    }

    @Test
    fun `Test emitter unsubscribe`() {
        val eventEmitter = EventEmitter<Boolean>()

        var testValue = false
        val callback: (it: Boolean) -> Unit = {
            testValue = it
        }
        eventEmitter.subscribe(callback)

        assertEquals(false, testValue)
        eventEmitter.nextSync(true)
        assertEquals(true, testValue)
        eventEmitter.unsubscribe(callback)
        eventEmitter.nextSync(false)
        assertEquals(true, testValue)
    }

    @Test
    fun `Test error catching on sync event`() {
        val eventEmitter = EventEmitter<Boolean>()

        val subscription = eventEmitter.subscribe {
            throw Exception("Test exception")
        }

        assertThrows<Exception> {
            eventEmitter.nextSync(true)
        }

        var detectThrow = false
        subscription.catch {
            detectThrow = true
        }

        assertEquals(false, detectThrow)
        assertDoesNotThrow {
            eventEmitter.nextSync(true)
        }
        assertEquals(true, detectThrow)
    }

    @Test
    fun `Test error catching on async event`() {
        val eventEmitter = EventEmitter<Boolean>()

        var detectThrow = false
        eventEmitter.subscribe {
            throw Exception("Test exception")
        }.catch {
            detectThrow = true
        }

        assertEquals(false, detectThrow)
        assertDoesNotThrow {
            eventEmitter.next(true)
        }
        Thread.sleep(10)
        assertEquals(true, detectThrow)
    }
}
