package com.aitorgf.sleepy.types

import org.junit.jupiter.api.Test
import java.nio.ByteBuffer
import kotlin.test.assertEquals

class UInt128Test {
    @Test
    fun `New from Int`() {
        val testNum = 75634
        val uint1 = UInt128(0L, testNum.toLong())
        val uint2 = UInt128(testNum)

        assertEquals(uint1, uint2)

        val uint3 = UInt128(0L.inv(), 0L.inv() - (testNum - 1))
        val uint4 = UInt128(-testNum)

        assertEquals(uint3, uint4)
    }

    @Test
    fun `New from Long`() {
        val uint1 = UInt128(byteArrayOf(
            0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte(),
            0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte(),
            0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte()))
        val uint2 = UInt128(-1L)

        assertEquals(uint1, uint2)
    }

    @Test
    fun `New from ByteArray`() {
        val bytes = byteArrayOf(
            0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte(),
            0xff.toByte(), 0xff.toByte(), 0x65.toByte(), 0x98.toByte(), 0xff.toByte(), 0xff.toByte(),
            0xff.toByte(), 0xff.toByte(), 0xff.toByte(), 0xff.toByte())
        val uint1 = UInt128(bytes)
        val uint2 = UInt128(bytes.getLongAt(0), bytes.getLongAt(8))

        assertEquals(uint1, uint2)
    }

    @Test
    fun `Plus other UInt128`() {
        val uint1 = UInt128(0L, 36L)
        val uint2 = UInt128(54)

        assertEquals(90, (uint1 + uint2).toInt())
    }

    @Test
    fun `Plus other Int`() {
        val uint1 = UInt128(0L, 36L)

        assertEquals(90, (uint1 + 54).toInt())
    }

    @Test
    fun `Bit operations, shift left`() {
        assertEquals(UInt128(2L), UInt128(1L) shl 1)
        assertEquals(UInt128(72L), UInt128(36L) shl 1)
    }

    @Test
    fun `Bit operations, shift right`() {
        assertEquals(UInt128(1L), UInt128(2L) shr 1)
        assertEquals(UInt128(36L), UInt128(72L) shr 1)
    }

    @Test
    fun `Bit operations, set bit`() {
        val base = UInt128(21845)

        val newValue1 = base.setBit(3, true)
        assertEquals(UInt128(21853), newValue1)

        val newValue2 = base.setBit(15, true)
        assertEquals(UInt128(54613), newValue2)

        val newValue3 = base.setBit(0, true, startFromLeft = true)
        assertEquals(UInt128(Long.MIN_VALUE, 21845L), newValue3)
    }
}
