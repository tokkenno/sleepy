package com.aitorgf.sleepy.types

import io.ktor.utils.io.core.*
import kotlin.test.Test
import kotlin.test.assertEquals

class ByteArrayTest {
    @Test
    fun `Read short from byte array`() {
        val raw1 = byteArrayOf(2, 0)
        assertEquals(2, raw1.getShortAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(2, raw1.reversedArray().getShortAt(0, ByteOrder.BIG_ENDIAN))

        val raw2 = byteArrayOf(0x57u.toByte(), 0x7cu.toByte())
        assertEquals(31831, raw2.getShortAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(31831, raw2.reversedArray().getShortAt(0, ByteOrder.BIG_ENDIAN))

        val raw3 = byteArrayOf(0x57u.toByte(), 0xfcu.toByte())
        assertEquals(-937, raw3.getShortAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(-937, raw3.reversedArray().getShortAt(0, ByteOrder.BIG_ENDIAN))

        val raw4 = byteArrayOf(0x57u.toByte(), 0x7cu.toByte())
        assertEquals(31831u, raw4.getUShortAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(31831u, raw4.reversedArray().getUShortAt(0, ByteOrder.BIG_ENDIAN))

        val raw5 = byteArrayOf(0x57u.toByte(), 0xfcu.toByte())
        assertEquals(64599u, raw5.getUShortAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(64599u, raw5.reversedArray().getUShortAt(0, ByteOrder.BIG_ENDIAN))
    }

    @Test
    fun `Read int from byte array`() {
        val raw1 = byteArrayOf(2, 0, 0, 0)
        assertEquals(2, raw1.getIntAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(2, raw1.reversedArray().getIntAt(0, ByteOrder.BIG_ENDIAN))

        val raw2 = byteArrayOf(0x57u.toByte(), 0x7cu.toByte(), 0x86u.toByte(), 0x20u.toByte())
        assertEquals(545684567, raw2.getIntAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(545684567, raw2.reversedArray().getIntAt(0, ByteOrder.BIG_ENDIAN))

        val raw3 = byteArrayOf(0x57u.toByte(), 0x7cu.toByte(), 0x86u.toByte(), 0xa0u.toByte())
        assertEquals(-1601799081, raw3.getIntAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(-1601799081, raw3.reversedArray().getIntAt(0, ByteOrder.BIG_ENDIAN))

        val raw4 = byteArrayOf(0x57u.toByte(), 0x7cu.toByte(), 0x86u.toByte(), 0x20u.toByte())
        assertEquals(545684567u, raw4.getUIntAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(545684567u, raw4.reversedArray().getUIntAt(0, ByteOrder.BIG_ENDIAN))

        val raw5 = byteArrayOf(0x57u.toByte(), 0x7cu.toByte(), 0x86u.toByte(), 0xa0u.toByte())
        assertEquals(2693168215u, raw5.getUIntAt(0, ByteOrder.LITTLE_ENDIAN))
        assertEquals(2693168215u, raw5.reversedArray().getUIntAt(0, ByteOrder.BIG_ENDIAN))
    }
}
